#include <Arduino.h>

#include "Defines.h"
#include "Includes.h"
#include "Machine.h"

Button *button_activity;
Button *button_on;
Button *button_rest;
Buttons *buttons;

BuzzerActive *buzzer;

Clock *clock_activity;
Clock *clock_global;
Clock *clock_rest;
Clocks *clocks;

Display *display_global;
Display *display_others;
Displays *displays;

Led *led_activity;
Led *led_rest;
Led *led_global;
Leds *leds;

Machine *state_machine;

void setup()
{ 
    buzzer = new BuzzerActive(SEG_BUZZER_KY012, DELAY_BUZZER);
    buzzer->pulse();

    button_activity = new ButtonBuzzer(SEG_BTTN_ACTIVITY, buzzer);
    button_on = new ButtonBuzzer(SEG_BTTN_ON, buzzer);
    button_rest = new ButtonBuzzer(SEG_BTTN_REST, buzzer);
    buttons = new Buttons(button_activity, button_on, button_rest);
    
    clock_activity = new ClockBuzzer(buzzer);
    clock_global = new ClockBuzzer(buzzer);
    clock_rest = new ClockBuzzer(buzzer);
    clocks = new Clocks(clock_activity, clock_global, clock_rest);
    
    display_global = new Display(SEG_CLK_GLOBAL, SEG_DIO_GLOBAL);
    display_others = new Display(SEG_CLK_OTHERS, SEG_DIO_OTHERS);
    displays = new Displays(display_global, display_others);

    led_activity = new Led(SEC_LED_ACTIVITY);
    led_rest = new Led(SEC_LED_REST);
    led_global = new Led(SEC_LED_ON);
    leds = new Leds(led_activity, led_rest, led_global);
    
    state_machine = new Machine(buttons, clocks, displays, leds);
}

void loop()
{
    display_global->clear();
    display_others->clear();

    clock_activity->time();
    clock_global->time();
    clock_rest->time();
  
    state_machine->check_state();
    state_machine->output();
     
    delay(DELAY_TIME);
}
