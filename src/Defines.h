#ifndef Defines_h
#define Defines_h

#define DELAY_TIME 999

#define SEG_BTTN_ACTIVITY  7
#define SEG_BTTN_REST      8
#define SEG_BTTN_ON        6

#define SEG_CLK_GLOBAL  3
#define SEG_DIO_GLOBAL  2
#define SEG_CLK_OTHERS  4
#define SEG_DIO_OTHERS  5

#define SEC_LED_ACTIVITY  10
#define SEC_LED_REST      11
#define SEC_LED_ON        9

#define SEG_BUZZER_KY012  12
#define DELAY_BUZZER      50

#endif // Defines_h
