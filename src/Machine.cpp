#include "Machine.h"

Machine::Machine(Buttons* buttons, Clocks *clocks, Displays *displays, Leds *leds)
{
    this->buttons = buttons;
    this->clocks = clocks;
    this->displays = displays;
    this->leds = leds;
    
    this->current_state = State::DISPLAY_OFF;
}

Input Machine::check_input()
{
    if (this->buttons->activity->pushed()) {
        return Input::PUSHED_ACTIVITY;
    } else if (this->buttons->on->pushed()) {
        return Input::PUSHED_ON;
    } else if (this->buttons->rest->pushed()) {
        return Input::PUSHED_REST;
    } else {
        return Input::NON_PUSHED;
    }
}

void Machine::check_state()
{
    Input input = this->check_input();

    switch (input) {
        case (PUSHED_ACTIVITY):
            switch (this->current_state) {
                case (DISPLAY_ACTIVITY):
                    this->current_state = State::DISPLAY_GLOBAL; break;
                case (DISPLAY_GLOBAL):
                    this->current_state = State::DISPLAY_ACTIVITY; break;
                case (DISPLAY_REST):
                    this->current_state = State::DISPLAY_ACTIVITY; break;
                case (DISPLAY_OFF):
                    this->current_state = State::DISPLAY_OFF; break;
            }
            break;
        case (PUSHED_ON):
            switch (this->current_state) {
                case (DISPLAY_ACTIVITY):
                case (DISPLAY_REST):
                case (DISPLAY_OFF):
                    this->current_state = State::DISPLAY_GLOBAL; break;
                case (DISPLAY_GLOBAL):
                    this->current_state = State::DISPLAY_OFF; break;
            }
            break;
        case (PUSHED_REST):
            switch (this->current_state) {
                case (DISPLAY_ACTIVITY):
                    this->current_state = State::DISPLAY_REST; break;
                case (DISPLAY_GLOBAL):
                    this->current_state = State::DISPLAY_REST; break;
                case (DISPLAY_REST):
                    this->current_state = State::DISPLAY_GLOBAL; break;
                case (DISPLAY_OFF):
                    this->current_state = State::DISPLAY_OFF; break;
            }
            break;
        case (NON_PUSHED):
        default:
          this->current_state = this->current_state;
    }
}

void Machine::output()
{
    switch (this->current_state) {
        case (DISPLAY_ACTIVITY):
            this->output_activity(); break;
        case (DISPLAY_GLOBAL):
            this->output_global(); break;
        case (DISPLAY_OFF):
            this->output_off(); break;
        case (DISPLAY_REST):
            this->output_rest(); break;
    }
}

void Machine::output_activity()
{   
    this->clocks->global->resume();
    this->clocks->activity->resume();
    this->clocks->rest->reset();
    
    this->displays->global->display(this->clocks->global);
    this->displays->others->display(this->clocks->activity);
    
    this->leds->activity->switch_on();
    this->leds->global->switch_on();
    this->leds->rest->switch_off();
}

void Machine::output_global()
{
    this->clocks->global->resume();
    
    this->clocks->activity->reset();
    this->clocks->rest->reset();
    
    this->clocks->activity->stop();
    this->clocks->rest->stop();

    this->displays->global->display(this->clocks->global);
    this->displays->others->clear();
        
    this->leds->activity->switch_off();
    this->leds->global->switch_on();
    this->leds->rest->switch_off();
}

void Machine::output_off()
{
    this->clocks->activity->reset();
    this->clocks->global->reset();
    this->clocks->rest->reset();
    
    this->displays->global->clear();
    this->displays->others->clear();

    this->leds->activity->switch_off();
    this->leds->global->switch_off();
    this->leds->rest->switch_off();
}

void Machine::output_rest()
{
    this->clocks->global->stop();
    this->clocks->activity->reset();
    this->clocks->rest->resume();
    
    this->displays->global->display(this->clocks->global);
    this->displays->others->display(this->clocks->rest);
    
    this->leds->activity->switch_off();
    this->leds->global->switch_on();
    this->leds->rest->switch_on();
}
