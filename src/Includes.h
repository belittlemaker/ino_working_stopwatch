#ifndef Include_h
#define Include_h

#include "devices/Button.h"
#include "devices/ButtonBuzzer.h"
#include "devices/BuzzerActive.h"
#include "devices/Clock.h"
#include "devices/ClockBuzzer.h"
#include "devices/Display.h"
#include "devices/Led.h"

#include "devices/Buttons.h"
#include "devices/Clocks.h"
#include "devices/Displays.h"
#include "devices/Leds.h"

#endif