#ifndef Displays_h
#define Displays_h

#include <Arduino.h>
#include "Display.h"

class Displays
{      
    public:
        Displays(Display *global, Display *others);
        Display *global;
        Display *others;
};

#endif // Displays_h
