#ifndef Clock_h
#define Clock_h

#include <Arduino.h>
#include <Time.h>
#include <TimeLib.h>

class Clock
{      
    public:
        Clock();
        int getHours();
        int getMinutes();
        int getSeconds();
        void reset();
        void resume();
        void stop();
        virtual void time();
    protected:
        unsigned long old_time;
        unsigned long pause_time;
        bool pause;
        int hours;
        int minutes;
        int seconds;
        void check_limit_time();
};

#endif // Clock_h
