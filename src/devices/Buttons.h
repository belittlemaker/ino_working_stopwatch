#ifndef Buttons_h
#define Buttons_h

#include <Arduino.h>
#include "Button.h"

class Buttons
{      
    public:
        Buttons(Button *activity, Button *on, Button *rest);
        Button *activity;
        Button *on;
        Button *rest;
};

#endif // Buttons_h
