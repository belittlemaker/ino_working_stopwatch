#include "ButtonBuzzer.h"

ButtonBuzzer::ButtonBuzzer(int pin, BuzzerActive *buzzer) : Button(pin)
{
    this->buzzer = buzzer;
}

bool ButtonBuzzer::pushed()
{
    bool pushed = Button::pushed();
    
    if (pushed) {
        this->buzzer->pulse();
    }

    return pushed;
}