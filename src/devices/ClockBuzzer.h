#ifndef ClockBuzzer_h
#define ClockBuzzer_h

#include <Arduino.h>
#include <Time.h>
#include <TimeLib.h>

#include "BuzzerActive.h"
#include "Clock.h"

class ClockBuzzer : public Clock
{      
    public:
        ClockBuzzer(BuzzerActive *buzzer);
        void time();
    private:
        BuzzerActive *buzzer;
        void alarm(int new_value, int last_value);
};

#endif // ClockBuzzer_h