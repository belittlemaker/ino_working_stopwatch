#include "Clock.h"

Clock::Clock()
{
    this->reset();
}

int Clock::getHours()
{
    return this->hours;
}

int Clock::getMinutes()
{
    return this->minutes;
}

int Clock::getSeconds()
{
    return this->seconds;
}

void Clock::reset()
{   
    this->seconds = 0;
    this->minutes = 0;
    this->hours = 0;
    this->old_time = millis();
    this->pause_time = 0;
    this->pause = true;
}

void Clock::resume()
{
    if (this->pause) {
        this->old_time = millis() - this->pause_time;
    }
    this->pause = false;
}

void Clock::stop()
{
    if (!this->pause) {
        this->pause_time = millis() - this->old_time;
    }
    this->pause = true;
}

void Clock::time()
{   
    if (!this->pause) {
        this->check_limit_time();
        
        unsigned long seconds = (millis() - this->old_time) / 1000;
        int seconds_remaining = seconds % 3600;
        
        this->hours = seconds / 3600;
        this->minutes = seconds_remaining / 60;
        this->seconds = seconds_remaining % 60;
    }
}

void Clock::check_limit_time()
{
    if (this->hours > 99) {
        this->old_time = millis();
    }
}
