#ifndef BuzzerActive_h
#define BuzzerActive_h

#include <Arduino.h>

class BuzzerActive
{      
    public:
        BuzzerActive(int pin, int delay_time);
        void pulse();
    private:
        int pin;
        int delay_time;
};

#endif // BuzzerActive_h