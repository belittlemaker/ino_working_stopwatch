#ifndef ButtonBuzzer_h
#define ButtonBuzzer_h

#include <Arduino.h>

#include "Button.h"
#include "BuzzerActive.h"

class ButtonBuzzer : public Button
{      
    public:
        ButtonBuzzer(int pin, BuzzerActive *buzzer);
        bool pushed();
    private:
        BuzzerActive *buzzer;
};

#endif // ButtonBuzzer_h