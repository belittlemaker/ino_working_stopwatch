#ifndef Button_h
#define Button_h

#include <Arduino.h>

class Button
{      
    public:
        Button(int pin);
        virtual bool pushed();
    private:
        int pin;
        int state;
        int read_state();
};

#endif // Button_h
