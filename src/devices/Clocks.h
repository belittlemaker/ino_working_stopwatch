#ifndef Clocks_h
#define Clocks_h

#include <Arduino.h>
#include "Clock.h"

class Clocks
{      
    public:
        Clocks(Clock *activity, Clock *global, Clock *rest);
        Clock *activity = new Clock();
        Clock *global = new Clock();
        Clock *rest = new Clock();
};

#endif // Clocks_h
