#ifndef Display_h
#define Display_h

#include <Arduino.h>
#include <TM1637.h>
#include "Clock.h"

class Display
{      
    public:
        Display(int clk, int dio);
        void clear();
        void display(Clock *clock);
    private:
        TM1637 *tm1637;
        String getOutput(int fisrt_segment, int second_segment);
        String normalizeDigit(int number);
};

#endif // Display_h
