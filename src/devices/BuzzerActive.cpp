#include "BuzzerActive.h"

BuzzerActive::BuzzerActive(int pin, int delay_time)
{
    this->pin = pin;
    this->delay_time = delay_time;
    
    pinMode (this->pin, OUTPUT);
}

void BuzzerActive::pulse()
{
    delay(this->delay_time);
    digitalWrite (this->pin, HIGH);
    delay(this->delay_time);
    digitalWrite (this->pin, LOW);
}