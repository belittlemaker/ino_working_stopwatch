#ifndef Leds_h
#define Leds_h

#include <Arduino.h>
#include "Led.h"

class Leds
{      
    public:        
        Leds(Led *activity, Led *rest, Led *global);
        Led *activity;
        Led *rest;
        Led *global;
};

#endif // Leds_h
