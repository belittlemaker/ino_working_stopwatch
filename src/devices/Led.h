#ifndef Led_h
#define Led_h

#include <Arduino.h>

class Led
{      
    public:        
        Led(int pin);
        void switch_on();
        void switch_off();
    private:
        int pin; 
};

#endif // Led_h
