#include "ClockBuzzer.h"

ClockBuzzer::ClockBuzzer(BuzzerActive *buzzer) : Clock()
{
    this->buzzer = buzzer;
}

void ClockBuzzer::time()
{   
    if (!this->pause) {

        int last_hour = this->getHours();

        Clock::time();

        int new_hour = this->getHours();

        this->alarm(new_hour, last_hour); 
    }
}

void ClockBuzzer::alarm(int new_value, int last_value)
{
    if (last_value < new_value) {
            this->buzzer->pulse();
            this->buzzer->pulse();
        }
}