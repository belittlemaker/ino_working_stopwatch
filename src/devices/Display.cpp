#include "Display.h"

Display::Display(int clk, int dio)
{   
    this->tm1637 = new TM1637(clk, dio);
    this->tm1637->begin();
}

void Display::clear()
{
    this->tm1637->display("  .  ");
}

void Display::display(Clock *clock)
{
    String output = (clock->getHours() > 0)
      ? this->getOutput(clock->getHours(),   clock->getMinutes())
      : this->getOutput(clock->getMinutes(), clock->getSeconds());

    this->tm1637->display(output);
}

String Display::getOutput(int fisrt_segment, int second_segment)
{
    return this->normalizeDigit(fisrt_segment) + "." + this->normalizeDigit(second_segment);
}

String Display::normalizeDigit(int number)
{
    return (number < 10) ? "0" + String(number) : String(number);
}
