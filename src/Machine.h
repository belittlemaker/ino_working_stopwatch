#ifndef Machine_h
#define Machine_h

#include "Defines.h"
#include "Includes.h"

enum State
{
    DISPLAY_ACTIVITY,
    DISPLAY_GLOBAL,
    DISPLAY_OFF,
    DISPLAY_REST,
};

enum Input
{
    PUSHED_ACTIVITY,
    PUSHED_ON,
    PUSHED_REST,
    NON_PUSHED,
};

class Machine
{      
    public:
        Machine(
            Buttons* buttons,
            Clocks *clocks,
            Displays *displays,
            Leds *leds
        );
        void check_state();
        void output();
    private:
        Buttons* buttons;
        Clocks *clocks;
        Displays *displays;
        Leds *leds;
        State current_state;
        Input check_input();
        void output_activity();
        void output_global();
        void output_off();
        void output_rest();
};

#endif // Machine_h
