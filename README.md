# ino_working_stopwatch
Arduino time management for work at home

![](assets/img/design.png)

Project
-------
- Build in Platformio for Visual Studio Code
  - https://platformio.org/install/ide?install=vscode
  - Physical components:
    - x1 Arduino Nano 
    - x3 Leds (blue, yellow, red)
    - x3 Switches/buttons (with covers: blue, yellow & red)
    - x6 Resistors:
      - x3 220 OHM (Leds)
      - x3 10 KILOHM (Switches)
    - x1 PCB: 50x70mm & 2.54mm
    - x2 Headres: x16
    - x2 TM1637: 4digits 7segments

Steps before
------------
Review the configuration files & choose your own configuration:
- Buttons & Leds pins
  - src/Defines.h

Functionality
-------------
Switches:
- Blue (1º Clock - Blue Led):
  - Init general time
  - Return Yellow & Red
  - Stop general time
- Yellow (2º Clock - Yellow Led):
  - Init Activity time
  - Stop Activity time
  - without stopping general time
- Red (2º Clock - Red Led):
  - Init Pause time
  - Stop Pause time
  - Stopping general time
